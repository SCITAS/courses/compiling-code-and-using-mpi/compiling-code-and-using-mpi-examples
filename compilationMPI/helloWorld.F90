! Main program
program main
  use mpi_f08

  implicit none

  integer :: world_rank, world_size, ierror
   
  ! Initialize the MPI environment
  call mpi_init(ierror)

  ! Get the number of processes and rank
  call mpi_comm_size(MPI_COMM_WORLD, world_size, ierror)
  call mpi_comm_rank(MPI_COMM_WORLD, world_rank, ierror)

  write(*, '(A,I0,A,I0)') 'Hello world from rank ', world_rank, " out of ", world_size

  call mpi_finalize(ierror)
end program main
