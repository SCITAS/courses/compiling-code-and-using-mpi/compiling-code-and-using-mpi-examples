module saxpy_mod
  use iso_fortran_env, only: dp=>real64

  implicit none

  private
  public :: saxpy

contains 

  subroutine saxpy(a, x, y, z)
    real(dp), intent(in) :: a
    real(dp), intent(in) :: x(:), y(:)
    real(dp), intent(inout) :: z(:)

    integer :: i

    do i = 1, size(z)
       z(i) = a * x(i) + y(i)
    enddo
  end subroutine saxpy
end module saxpy_mod

