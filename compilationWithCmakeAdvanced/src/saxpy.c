#include <stdio.h>

void saxpy(int n, double a, double* x, double* y, double* z) {
  for(int i = 0; i < n; i++)
  {
    z[i] = a*x[i] + y[i];
  }
}



double maxValue(double myArray[], int size) {
    int i;
    double maxValue = myArray[0];

    for (i = 1; i < size; ++i) {
      if ( myArray[i] > maxValue ) {
	maxValue = myArray[i];
      }
    }
    return maxValue;
}

