! saxpy routine
module saxpy_mod
  use iso_fortran_env, only: di=>int64, dp=>real64
contains 
subroutine saxpy(n, a, x, y, z)
  integer, intent(in) :: n
  real(dp), intent(in) :: x(:), y(:), a
  real(dp), intent(out) :: z(:)
  integer :: i

  do i=1,n
     z(i) = a*x(i)+y(i)
  enddo
end subroutine saxpy

end module saxpy_mod

